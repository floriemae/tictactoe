package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
public class DB {
	
	public static boolean insertResults(String playername, String character){
		Connection con = DBConnection.DBConnect();
		PreparedStatement ps = null;
		try {
			con = DBConnection.DBConnect();
			ps = con.prepareStatement("INSERT INTO gameresults (PlayerName, AssignedCharacter, DateTimePlayed, Result) VALUES (?,?,NOW(), ?)");
			ps.setString(1, playername);
			ps.setString(2, character);
			ps.setString(3, "W");
			if(ps.executeUpdate() > 0) {
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			if(con != null) {try{con.close();}catch(Exception ex) {ex.printStackTrace();}}
		}
		
		return false;
	}
	
	public static String scoreboard() {
		String res = "";
		Connection con = DBConnection.DBConnect();
		PreparedStatement ps = null;
		try {
			con = DBConnection.DBConnect();
			ps = con.prepareStatement("SELECT PlayerName, COUNT(PlayerName) as score FROM gameresults GROUP BY PlayerName ORDER BY COUNT(PlayerName) DESC");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res += "<tr><td>"+rs.getString(1)+"</td><td>"+rs.getString(2)+"</td></tr>";
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			if(con != null) {try{con.close();}catch(Exception ex) {ex.printStackTrace();}}
		}
		
		return res;
	}

}
